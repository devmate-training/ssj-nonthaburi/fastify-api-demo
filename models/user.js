module.exports = {
	read(db, limit, skip) {
		return db('users')
			.select('username', 'first_name', 'last_name', 'role', 'user_id')
			.limit(limit)
			.offset(skip);
	},

	total(db) {
		return db('users').count('user_id as total');
	},

	create(db, user) {
		return db('users').insert(user);
	},

	getInfo(db, username) {
		return db('users').where('username', username).first();
	},
};
