'use strict';

const fp = require('fastify-plugin');

module.exports = fp(async function (fastify, opts) {
	fastify.register(require('fastify-guard'), {
		errorHandler: (result, request, reply) => {
			return reply.status(405).send({
				statusCode: 405,
				error: 'You are not allowed to call this route',
			});
		},
	});
});
