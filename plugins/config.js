'use strict';

const fp = require('fastify-plugin');

module.exports = fp(async function (fastify, opts) {
	fastify.register(require('@fastify/env'), {
		dotenv: {
			path: `./config/.env`,
			debug: true,
		},
		schema: {
			type: 'object',
			properties: {
				NODE_ENV: {
					type: 'string',
					default: 'development',
				},
				JWT_SECRET: {
					type: 'string',
					default: 'secret',
				},
				DB_HOST: {
					type: 'string',
					default: 'localhost',
				},
				DB_PORT: {
					type: 'number',
					default: 3306,
				},
				DB_USER: {
					type: 'string',
					default: 'root',
				},
				DB_PASSWORD: {
					type: 'string',
					default: '123456',
				},
				DB_NAME: {
					type: 'string',
					default: 'test',
				},
			},
		},
	});
});
