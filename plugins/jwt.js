'use strict';

const fp = require('fastify-plugin');

module.exports = fp(async function (fastify, opts) {
	fastify.register(require('@fastify/jwt'), {
		secret: process.env.JWT_SECRET || 'xxxxxx',
		sign: {
			iss: process.env.JWT_ISSUER || 'test.satit.dev',
			expiresIn: '1d',
		},
	});

	fastify.decorate('authenticate', async function (request, reply) {
		try {
			await request.jwtVerify();
		} catch (err) {
			reply.send(err);
		}
	});
});
