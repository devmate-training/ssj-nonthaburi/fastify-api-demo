'use strict';

/** @type {import('fluent-json-schema').default} */
const S = require('fluent-json-schema');

const userModel = require('../models/user');
const hash = require('../models/hash');

module.exports = async function (fastify, opts) {
	const db = fastify.db;

	fastify.post(
		'/login',
		{
			schema: {
				body: S.object()
					.prop(
						'username',
						S.string().minLength(3).maxLength(50).required()
					)
					.prop(
						'password',
						S.string().minLength(4).maxLength(50).required()
					),
			},
		},
		async function (request, reply) {
			const { username, password } = request.body;
			try {
				// Check username is exist
				const user = await userModel.getInfo(db, username);
				if (!user) {
					return reply
						.status(404)
						.send({ ok: false, message: 'User not found' });
				}
				const hashPassword = user.password;
				// Compare password
				const isMatch = await hash.comparePassword(
					password,
					hashPassword
				);
				if (!isMatch) {
					return reply
						.status(401)
						.send({ ok: false, message: 'Wrong password' });
				}

				const roles = user.role.split(',');
				// Generate jwt token
				const token = await fastify.jwt.sign({
					sub: user.user_id,
					role: roles,
				});

				// console.log(token);

				return reply.send({ token });
			} catch (error) {
				console.log(error);
				return reply
					.status(500)
					.send({ ok: false, message: 'Something went wrong' });
			}
		}
	);
};
