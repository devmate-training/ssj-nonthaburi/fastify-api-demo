'use strict';

const S = require('fluent-json-schema');

module.exports = async function (fastify, opts) {
	const db = fastify.db;

	fastify.get('/', async function (request, reply) {
		const users = await db('users').select(); // SELECT * FROM users;
		return reply.send(users);
	});

	fastify.get('/new', async function (request, reply) {
		const data = [
			{
				first_namez: 'xxxx',
				last_name: 'xxxxxx',
				role: 'admin',
				username: 'abxdsfsdf',
				password: 'sfjlksjflsfj',
			},
			{
				first_name: 'xxxsfsdfx',
				last_name: 'sdfsdf',
				role: 'admin',
				username: 'sdfsdfsd',
				password: 'sdfsdfsd',
			},
		];

		data.forEach(async (user) => {
			await db('users').insert(user);
		});
	});

	fastify.get('/transaction', async function (request, reply) {
		try {
			await db.transaction(async (trx) => {
				await trx('users').insert([
					{
						first_namez: 'xxxx',
						last_name: 'xxxxxx',
						role: 'admin',
						username: 'abxdsfsdf',
						password: 'sfjlksjflsfj',
					},
					{
						first_name: 'xxxsfsdfx',
						last_name: 'sdfsdf',
						role: 'admin',
						username: 'sdfsdfsd',
						password: 'sdfsdfsd',
					},
				]);

				await trx('users').where('user_id', 1).del();
			});

			return reply.send({ ok: true });
		} catch (error) {
			console.log(error);
			return reply.status(500).send();
		}
	});

	//  http://localhost:3000/test/info?userId=xx
	fastify.get('/info', async function (request, reply) {
		const { userId } = request.query;
		const sql = `SELECT * FROM users WHERE user_id=?`;
		const user = await db.raw(sql, [userId]);
		console.log(user);

		return reply.send({ info: user[0] });
	});
	//  http://localhost:3000/test/xx/delete
	fastify.delete('/:userId/delete', async function (request, reply) {
		const { userId } = request.params;
		await db('users').where('user_id', userId).del();
		return reply.send({ ok: true });
	});

	fastify.put(
		'/:userId/edit',
		{
			schema: {
				body: S.object()
					.prop('firstName', S.string().minLength(1).maxLength(50))
					.prop('lastName', S.string().minLength(1).maxLength(50)),
			},
		},
		async function (request, reply) {
			const { userId } = request.params;
			const { firstName, lastName } = request.body;

			try {
				await db('users').where('user_id', userId).update({
					first_name: firstName,
					last_name: lastName,
				});

				return reply.send({ ok: true });
			} catch (error) {
				console.log(error);
				return reply.status(500).send('Internal server error');
			}
		}
	);
};
