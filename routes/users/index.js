'use strict';
/** @type {import('fluent-json-schema').default} */
const S = require('fluent-json-schema');

const hash = require('../../models/hash');
const userModel = require('../../models/user');

module.exports = async function (fastify, opts) {
	fastify.get(
		'/',
		{
			onRequest: [fastify.authenticate],
		},
		async function (request, reply) {
			const { limit, skip } = request.query;

			const users = await userModel.read(fastify.db, limit, skip);
			const total = await userModel.total(fastify.db);
			return reply.send({ users, total: total[0].total });
		}
	);

	fastify.post(
		'/',
		{
			onRequest: [fastify.authenticate],
			preHandler: [fastify.guard.role('user')],
			schema: {
				body: S.object()
					.prop(
						'username',
						S.string().minLength(1).maxLength(50).required()
					)
					.prop(
						'password',
						S.string().minLength(4).maxLength(50).required()
					)
					.prop(
						'firstName',
						S.string().minLength(1).maxLength(50).required()
					)
					.prop(
						'lastName',
						S.string().minLength(1).maxLength(50).required()
					)
					.prop(
						'role', // admin,staff,user
						S.string().required()
					),
			},
		},
		async function (request, reply) {
			const { username, password, firstName, lastName, role } =
				request.body;

			try {
				// Hash password
				const hashedPassword = await hash.hashPassword(password);

				const user = {
					username,
					password: hashedPassword,
					first_name: firstName,
					last_name: lastName,
					role,
				};

				await userModel.create(fastify.db, user);
				return reply
					.status(201)
					.send({ ok: true, message: 'User created' });
			} catch (error) {
				console.log(error);
				return reply
					.status(500)
					.send({ ok: false, message: 'Error creating user' });
			}
		}
	);
};
