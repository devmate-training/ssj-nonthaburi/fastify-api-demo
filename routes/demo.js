'use strict';

/** @type {import('fluent-json-schema').default} */
const S = require('fluent-json-schema');

module.exports = async function (fastify, opts) {
	fastify.get(
		'/demo',
		{
			schema: {
				querystring: S.object()
					.prop(
						'limit',
						S.number().default(10).minimum(1).maximum(100)
					)
					.prop(
						'skip',
						S.number().default(0).minimum(0).maximum(500)
					),
			},
		},
		async function (request, reply) {
			const { limit, skip } = request.query;
			return { ok: true, message: 'GET', limit, skip };
		}
	);

	fastify.post(
		'/demo',
		{
			schema: {
				body: S.object()
					.prop(
						'firstName',
						S.string().minLength(1).maxLength(50).required()
					)
					.prop(
						'lastName',
						S.string().minLength(1).maxLength(50).required()
					),
			},
		},
		async function (request, reply) {
			const { firstName, lastName } = request.body;
			return { ok: true, message: 'POST', firstName, lastName };
		}
	);

	fastify.put(
		'/demo/:userId/:departmentId/edit',
		{
			schema: {
				params: S.object()
					.prop('userId', S.number().required())
					.prop('departmentId', S.number().required()),
				body: S.object()
					.prop(
						'firstName',
						S.string().minLength(1).maxLength(50).required()
					)
					.prop(
						'lastName',
						S.string().minLength(1).maxLength(50).required()
					)
					.prop('birthDate', S.string().format('date').required()),
			},
		},
		async function (request, reply) {
			const { userId, departmentId } = request.params;
			const { firstName, lastName, birthDate } = request.body;
			return {
				ok: true,
				message: 'PUT',
				userId,
				departmentId,
				firstName,
				lastName,
				birthDate,
			};
		}
	);

	fastify.delete('/demo', async function (request, reply) {
		return { ok: true, message: 'DELETE' };
	});
};
